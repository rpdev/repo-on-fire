# Argument Injection

The `repo` tool is often used directly by you as a user. But... what if not?
For example, there are plugins for the Jenkins CI/CD platform which can call
`repo` for you to set up a workspace to run the pipeline steps in.

Unfortunately, sometimes such integrations don't allow fine tuning all the
necessary aspects you might require. For example, you might need to pass the
`--force-remove-dirty` option to a `repo sync` call of the above said plugin.
But what if the plugin does not allow you to add this option?

When using `repo-on-fire` as a wrapper for `repo`, injecting additional
arguments is a breeze! Let's see how to do it.


## Injecting Arguments Via The Config File

You can inject additional arguments for native `repo` calls by adding them
to the config file. First, locate the path to the config file to be used:

```bash
repo-on-fire config path
```

This prints the path to the default config file, which differs between
platforms, for example `/home/tux/.config/RepoOnFire/config.toml`.

!!! note
    You can set the `REPO_ON_FIRE_CONFIG_FILE_PATH` environment variable to
    force the tool to read from this file instead.

Open this file in an editor of your choice. We need to add the
`native_command_additional_arguments` option. This option is a map where the
keys are the native `repo` commands to which we want to add arguments and the
values are lists of arguments to add. If we want to include the
`--force-remove-dirty` option to each `repo sync` call, we should add the
following to the config file:

```toml
[native_command_additional_arguments]
sync = ["--force-remove-dirty"]
```

That's it! Save the file and run `sync` via repo-on-fire:

```bash
repo-on-fire sync
```

While not immediately visible, this now invokes `repo sync` with the additional
option we've specified.

!!! warning
    Be careful with the concrete options. The `--force-remove-dirty` option is
    actually a good example: If used carelessly, it can cause data loss in
    your workspace. In a CI/CD scenario, this option might be okay, but for
    _interactive use cases_, better add this flag as needed.
