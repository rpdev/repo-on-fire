# Welcome to repo-on-fire 🔥

`repo-on-fire` is a thin wrapper around Google's `repo` tool. The idea is
quite simple: Behave as much as `repo` as possible (and hence, don't get in the
user's way), but add some useful gimmicks here and there as needed!

In fact, `repo-on-fire` calls `repo` under the hood for getting most of the
work done. So most of the time, you should not be able to tell if you use one
or the other.


## First Steps

First things first: To use `repo-on-fire`, you have to install it one way or
the other. The [Installation](tutorials/installation.md) tutorial outlines
various ways to do so.

Once set up, `repo-on-fire` can be used as a drop-in replacement for the
`repo` tool. But... the tool would be quite boring if it didn't add anything
on top, right? So, you can follow up with some more specific tutorials
covering the additional features that the tool adds on top:

In [Workspace Caching](tutorials/workspace-caching.md), we show you how
`repo-on-fire` caches the creation of new workspaces and hence, can help you
speeding up this often time consuming step.

The [Workspace Manipulation](./tutorials/workspace-manipulation.md) tutorial
details the `workspace` command and how it can be used to easily alter an entire
workspace.


## More Questions?

If you feel the tutorials don't go far enough, feel free to dive into the
[Howtos](./howtos/index.md) section - it provides some more in-depth tips and
tricks on tweaks and usage of the tool!


## Getting To The Roots

You want to learn more about the general concepts of managing larger `git` based
projects? Want to understand the concepts on which `repo-on-fire` is built?
Then follow up in the [Explanation](./explanation/index.md) section.


## Even More Fun: Python APIs!

`repo-on-fire` is primarily a command line tool, true. But, being written in
Python, it also exposes some APIs that you can use to further automate your
workspace tasks. Interested? Then head over to the
[Reference](./reference/index.md) section.
