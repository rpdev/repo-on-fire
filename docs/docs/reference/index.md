# Reference

`repo-on-fire` is both a command line tool as well as a Python module
exposing some useful APIs that you can leverage to automate certain
workspace steps. In this section, you can find the following:

- An overview of the [Command Line Interface](./cli/index.md) that the
  tool provides.
- A reference documentation of the [Python API](./python_api/index.md) that you
  can use for advanced automation tasks.
