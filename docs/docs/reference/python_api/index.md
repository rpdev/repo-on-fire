# Python API

As `repo-on-fire` is written in Python, it also exposes a part of its
functionality as an API for your usage. So while `repo` (and hence,
`rpeo-on-fire`) already provide a rich command line interface for automating
a lots of common tasks on a larger workspace, you can go even further
utilizing the Python based API for more advances tasks.
