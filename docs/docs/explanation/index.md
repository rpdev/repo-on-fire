# Explanation

*Why did you start writing `repo-on-fire`? How can it help me improving my
workflow? And what shouldn't I expect from the tool?* If these are the things
you're asking yourself - this section has you covered.

We'll describe some of the ideas and philosophy of the tool, gathering pros and
cons of using it and also point to other tools you might want to consider
instead, so you can make a sound decision if or if not to use it.
