"""Entry points for console."""

from .cli import cli


def main():
    """Main entry point."""
    cli()


if __name__ == "__main__":
    main()
